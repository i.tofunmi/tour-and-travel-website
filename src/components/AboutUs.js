import "../components/AboutUsStyles.css";

function AboutUs() {
  return (
    <div className="about-container">
      <h1>Our History</h1>
      <p>
      Welcome to the journey of Tof-Trips, where every click takes you on an adventure through time and places! 
      Born from the creative minds at React, Tof-Trips emerged as a beacon for wanderlust souls seeking seamless travel experiences. 
      Tof-Trips takes inspiration from the tapestry of human history, blending modern technology with the timeless allure of exploration. 
      As you navigate the website, each feature reflects the team's commitment to crafting a seamless travel companion.
      </p>

      <h1>Our Mission</h1>
      <p>
        Our mission is to touch the horizon where our capabilities may
        successfully meet with the requirements of our clients, that too with
        ultimate transparency and cost-effectiveness.
      </p>

      <h1>Our Vision</h1>
      <p>
        To sow the seeds of par-excellence services with customer centric
        approach and reap the trust of worldwide clients.
      </p>
    </div>
  );
}

export default AboutUs;
